# Define Terraform backend using a S3 bucket for storing the Terraform state
terraform {
  backend "s3" {
    bucket = "my-terraform-state-bucket125"
    key = "terraform-state/terraform.tfstate"
    region = "ap-south-1"
 }
}