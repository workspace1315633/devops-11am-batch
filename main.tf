
# Create a Security Group for an EC2 instance
resource "aws_security_group" "instance" {
  name = "terraform-ec2-instance-sg"
  
  ingress {
    from_port	  = 80
    to_port	      = 80
    protocol	  = "tcp"
    cidr_blocks	= ["0.0.0.0/0"]
  }
  ingress {
    from_port	  = 22
    to_port	      = 22
    protocol	  = "tcp"
    cidr_blocks	= ["0.0.0.0/0"]
  }
  egress {
    from_port	  = 0
    to_port	      = 0
    protocol	  = "-1"
    cidr_blocks	= ["0.0.0.0/0"]
  }

}

# Create an EC2 instance
resource "aws_instance" "myinstance" {
  ami           = var.ami_name
  instance_type = var.instance_type
  vpc_security_group_ids  = ["${aws_security_group.instance.id}"]
  key_name = var.key_pair
  user_data = "${file("install_apache.sh")}"	  
  tags = {
    Name = "myec2"
  }
}


