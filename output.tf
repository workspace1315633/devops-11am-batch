# Output variable: Public IP address
output "public_ip" {
  value = aws_instance.myinstance.public_ip
}