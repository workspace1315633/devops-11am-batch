variable "ami_name" {
 type = string 
 description = "Ami of my Ec2"
}

variable "instance_type" {
 type = string 
 description = "instance type of my Ec2"
}

variable "key_pair" {
 type = string 
 description = "key pair of my Ec2"
}